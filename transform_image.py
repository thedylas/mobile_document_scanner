from transform import four_point_transform
import numpy
import argparse
import cv2

argument_parse = argparse.ArgumentParser()
argument_parse.add_argument("-i", "--image", help="Path to image file")
argument_parse.add_argument("-c", "--coordinates", help="comma separated list of points")
arguments = vars(argument_parse.parse_args())


image = cv2.imread(arguments["image"])
points = numpy.array(eval(arguments["coordinates"]), dtype="float32")


warped = four_point_transform(image, points)


cv2.imshow("Original", image)
cv2.imshow("Warped", warped)
cv2.waitKey(0)
