from transform import four_point_transform
from skimage.filters import threshold_local
import numpy
import argparse
import cv2
import imutils


argument_parse = argparse.ArgumentParser()
argument_parse.add_argument("-i", "--image", required=True, help="Path to image to be scanned")
arguments = vars(argument_parse.parse_args())


image = cv2.imread(arguments["image"])
ratio = image.shape[0] / 500.0
original = image.copy()
image = imutils.resize(image, height=500)


gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = cv2.GaussianBlur(gray, (5, 5), 0)
edged = cv2.Canny(gray, 75, 200)

print("Step 1: Edge detection")
cv2.imshow("Image", image)
cv2.imshow("Edged", edged)
cv2.waitKey(5000)
cv2.destroyAllWindows()


contours = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
contours = imutils.grab_contours(contours)
contours = sorted(contours, key=cv2.contourArea, reverse=True)[:5]

for c in contours:
    peri = cv2.arcLength(c, True)
    approximate = cv2.approxPolyDP(c, 0.02 * peri, True)

    if len(approximate) == 4:
        screenContour = approximate
        break

print("Step 2: Find contours")
cv2.drawContours(image, [screenContour], -1, (0, 255, 0), 2)
cv2.imshow("Outline", image)
cv2.waitKey(1000)
cv2.destroyAllWindows()

warped = four_point_transform(original, screenContour.reshape(4, 2) * ratio)

warped = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
T = threshold_local(warped, 11, offset=10, method="gaussian")
warped = (warped > T).astype("uint8") * 255

print("Step 3: Apply perspective transform")
cv2.imshow("Original", imutils.resize(original, height=650))
cv2.imshow("Scanned", imutils.resize(warped, height=650))
cv2.waitKey(0)
