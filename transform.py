import numpy
import cv2


def order_points(points):
	rectangle = numpy.zeros((4, 2), dtype="float32")

	s = points.sum(axis=1)
	rectangle[0] = points[numpy.argmin(s)]
	rectangle[2] = points[numpy.argmax(s)]

	diff = numpy.diff(points, axis=1)
	rectangle[1] = points[numpy.argmin(diff)]
	rectangle[3] = points[numpy.argmax(diff)]
	
	return rectangle
	
	
def four_point_transform(image, points):
	rectangle = order_points(points)
	(top_left, top_right, bottom_right, bottom_left) = rectangle
	
	bottom_width = numpy.sqrt(((bottom_right[0] - bottom_left[0]) ** 2) + ((bottom_right[1] - bottom_left[1]) ** 2))
	top_width = numpy.sqrt(((top_right[0] - top_left[0]) ** 2) + ((top_right[1] - top_left[1]) ** 2))
	max_width = max(int(top_width), int(bottom_width))
	
	left_height = numpy.sqrt(((top_left[0] - bottom_left[0]) ** 2) + ((top_left[1] - bottom_left[1]) ** 2))
	right_height = numpy.sqrt(((top_right[0] - bottom_right[0]) ** 2) + ((top_right[1] - bottom_right[1]) ** 2))
	max_height = max(int(left_height), int(right_height))
	
	destination = numpy.array([
		[0, 0],
		[max_width - 1, 0],
		[max_width - 1, max_height - 1],
		[0, max_height - 1]], dtype="float32")

	matrix = cv2.getPerspectiveTransform(rectangle, destination)
	warped = cv2.warpPerspective(image, matrix, (max_width, max_height))

	return warped
