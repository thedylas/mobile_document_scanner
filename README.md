# Document Scanner

This project is a very basic python program that takes in an image and creates a "scan" of it. It does this by detecting the corners/edges of the paper being scanned, and then warping the image to display a top-down view.

## Getting Started

1. Download the repository
2. Install the prerequisites listed below
3. In a terminal navigate to the folder with the .py files
4. Type in `python scan.py --image [path to image]`

### Prerequisites

There are a few libraries imported for this project that you may need to install:
- numpy `pip install numpy`
- cv2 `pip install opencv`
- argparse `pip install argparse`
- scikit-image `pip install scikit-image`
- imutils `pip install imutils`

## Built With

* [OpenCV](https://opencv.org/) - The computer vision library used
